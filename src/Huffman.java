
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   class Node {
      byte code;
      int codeLength = 0;
      int frequency;
      Node left;
      Node right;
      Byte symbol;

      Node(byte symbol, int frequency) {
         this.symbol = symbol;
         this.frequency = frequency;
      }

      Node(Node left, Node right) {
         this.frequency = left.frequency + right.frequency;
         this.left = left;
         this.right = right;
      }

      Node(Node left) {
         this.frequency = left.frequency;
         this.left = left;
      }
   }

   private Node root;
   private Map<Byte, Node> leaves;
   private int bitLength;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      makeTree(original);
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      return bitLength;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      byte[] encoded = new byte[origData.length];
      for (int i = 0; i < origData.length; i++) {
         Node leaf = leaves.get(origData[i]);
         encoded[i] = leaf.code;
         bitLength += leaf.codeLength;
      }
      return encoded;
   }

   private void makeTree(byte[] origData) {
      leaves = new HashMap<>();
      Map<Byte, Integer> characters = countCharacters(origData);
      ArrayList<Node> nodes = createNodes(characters);

      if (nodes.size() > 1) {
         while (nodes.size() > 1) {
            Node firstMinimum = nodes.get(0);
            Node secondMinimum = nodes.get(1);
            for (Node node : nodes) {
               if (node.frequency < firstMinimum.frequency) {
                  secondMinimum = firstMinimum;
                  firstMinimum = node;
               } else if (node.frequency < secondMinimum.frequency && node != firstMinimum) {
                  secondMinimum = node;
               }
            }

            Node internalNode = makeNode(firstMinimum, secondMinimum);
            nodes.remove(firstMinimum);
            nodes.remove(secondMinimum);
            nodes.add(internalNode);
         }
      }
      else {
         Node internalNode = new Node(nodes.get(0));
         updateLeft(internalNode.left);
         nodes.set(0, internalNode);
      }

      root = nodes.get(0);
   }

   private Node makeNode(Node left, Node right) {
      Node newNode = new Node(left, right);
      updateNode(left, 0);
      updateNode(right, 1);
      return newNode;
   }

   private void updateLeft(Node left) {
      updateNode(left, 0);
   }

   private void updateNode(Node node, int updateBit) {
      node.codeLength++;
      node.code <<= 1;
      node.code += updateBit;
      if(node.symbol == null) {
         updateNode(node.left, updateBit);
         if(node.right != null) {
            updateNode(node.right, updateBit);
         }
      }
   }

   private ArrayList<Node> createNodes(Map<Byte, Integer> characters) {
      ArrayList<Node> nodes = new ArrayList<>();
      for (Map.Entry<Byte, Integer> character : characters.entrySet()) {
         Node newNode = new Node(character.getKey(), character.getValue());
         nodes.add(newNode);
         leaves.put(character.getKey(), newNode);
      }
      return nodes;
   }

   private Map<Byte, Integer> countCharacters(byte[] origData) {
      Map<Byte, Integer> characters = new HashMap<>();

      for (byte character : origData) {
         if (characters.containsKey(character)) {
            characters.put(character, characters.get(character) + 1);
         }
         else {
            characters.put(character, 1);
         }
      }
      return characters;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      byte[] decodedData = new byte[encodedData.length];
      for (int i = 0; i < encodedData.length; i++) {
         decodedData[i] = decodeByte(encodedData[i], root);
      }
      return decodedData;
   }

   private byte decodeByte(byte leftToDecode, Node currentNode) {
      if (currentNode.symbol != null) {
         return currentNode.symbol;
      }
      else if (leftToDecode % 2 == 0) {
         leftToDecode >>= 1;
         return decodeByte(leftToDecode, currentNode.left);
      }
      else {
         leftToDecode >>= 1;
         return decodeByte(leftToDecode, currentNode.right);
      }
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }
}